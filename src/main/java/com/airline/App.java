package com.airline;

import com.airline.reservation.FlightReservationSystem;

import java.io.IOException;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        String pathForFlightInfo = "/home/zoheb/java/FlightReservation/src/res/in/inputfile1.txt";
        String pathForPersonDetail = "/home/zoheb/java/FlightReservation/src/res/in/inputfile2.txt";
        String output = "/home/zoheb/java/FlightReservation/src/res/in/outputfile.txt";
        if (args.length > 0) {
            pathForFlightInfo = args[0];
        }
        if (args.length > 0) {
            pathForPersonDetail = args[1];
        }
        if (args.length > 0) {
            output = args[2];
        }
        FlightReservationSystem flightReservationSystem = new FlightReservationSystem(pathForFlightInfo);
        flightReservationSystem.handleTranscation(pathForPersonDetail);
        try {
            flightReservationSystem.createOutput(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
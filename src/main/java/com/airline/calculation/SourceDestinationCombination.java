package com.airline.calculation;

public class SourceDestinationCombination {
    public SourceDestinationCombination(String source, String destination) {
        if (!(validAirportCode(source) && validAirportCode(destination))) {
            throw new IllegalArgumentException("Airport code invalid");
        }
    }

    private boolean validAirportCode(String airportCode) {
        if (airportCode == null || airportCode.length() != 3) {
            return false;
        }
        for (int i = 0; i < airportCode.length(); i++) {
            if (!isLetter(airportCode.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean isLetter(Character airportCodeChar) {
        return (airportCodeChar >= 'a' && airportCodeChar <= 'z'
                || airportCodeChar >= 'A' && airportCodeChar <= 'Z');
    }
}

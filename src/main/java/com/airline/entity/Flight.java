package com.airline.entity;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Flight implements Comparable<Flight> {
    private final String source;
    private final String destination;
    private final int seats;
    private final String flightNumber;
    Map<Passenger, ReservationItem> passengerReservationItemMap;
    List<Integer> ticketpool;
    private int price;

    public Flight(String source, String destination, int price,
                  int seats, String flightNumber, Map<Passenger,
            ReservationItem> passengerReservationItemMap, List<Integer> ticketpool) {
        this.source = source;
        this.destination = destination;
        this.price = price;
        this.seats = seats;
        this.flightNumber = flightNumber;
        this.passengerReservationItemMap = new HashMap<>();
        ticketpool = new LinkedList<>();
        for (int i = 0; i < seats; i++) {
            ticketpool.add(seats);
        }
    }

    public boolean isFull() {
        return passengerReservationItemMap.size() == seats;

    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public int getPrice() {
        return price;
    }

    public int getSeats() {
        return seats;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public Map<Passenger, ReservationItem> getPassengerReservationItemMap() {
        return passengerReservationItemMap;
    }

    public void bookPassenger(ReservationItem reservationItem) {
        if (passengerReservationItemMap.containsKey(reservationItem.getPassenger())) {
            return;
        } else {
            passengerReservationItemMap.put(reservationItem.getPassenger(), reservationItem);
        }
    }

    public void changePrice(int newPrice) {
        this.price = newPrice;
    }

    public int getPriceforSeat() {
        return price;
    }

    public int getRandomSeats() {
        int randomIndex = generateSeat(0, ticketpool.size() - 1);
        return ticketpool.remove(randomIndex);
    }

    private int generateSeat(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));

    }

    public ReservationItem getReservationByPassenger(Passenger passenger) {
        return passengerReservationItemMap.get(passenger);
    }

    public void cancelPassenger(ReservationItem reservationItem) {

        Passenger passenger = reservationItem.getPassenger();
        if (passenger != null) {
            if (passengerReservationItemMap.get(passenger) != null) {
                passengerReservationItemMap.remove(passenger);
            }
        }
    }

    public void recoverSeat(int seatnumber) {
        ticketpool.add(seatnumber);
    }

    public FlightSummary summaryFlight() {
        StringBuffer summaryBuffer = new StringBuffer();
        summaryBuffer.append("Flight# ")
                .append(flightNumber)
                .append(" Number of seats available: ")
                .append(getSeats())
                .append("\n")
                .append("Total seats sold: ")
                .append(passengerReservationItemMap.size())
                .append("\n");
        StringBuffer passengerInfoBuffer = new StringBuffer();
        String format = "%-50s %-10s %-10s";
        passengerInfoBuffer.append(String.format(format, "Passenger Name", "Seat#", "Price"))
                .append("\n");
        int totalAvenue = 0;
        for (Map.Entry<Passenger, ReservationItem> entry : passengerReservationItemMap.entrySet()) {
            Passenger passenger = entry.getKey();
            ReservationItem reservationItem = entry.getValue();
            passengerInfoBuffer.append(String.format(format, passenger.getName(),
                    reservationItem.getSeatnumber(),
                    "$" + reservationItem.getPrice()))
                    .append("\n");
            totalAvenue += reservationItem.getPrice();
        }
        summaryBuffer.append("Total revenue on this flight: $" + totalAvenue)
                .append("\n\n")
                .append(passengerInfoBuffer);
        return new FlightSummary(flightNumber, getSeats(), passengerReservationItemMap.size(),
                totalAvenue, summaryBuffer.toString());
    }

    @Override
    public int compareTo(Flight o) {
        return this.price - o.price;
    }


    public static class FlightBuilder {

        private String source;
        private String destination;
        private int price;
        private int seats;
        private String flightNumber;
        private Map<Passenger, ReservationItem> passengerReservationItemMap;
        private List<Integer> ticketpool;

        public FlightBuilder setSource(String source) {
            this.source = source;
            return this;
        }

        public FlightBuilder setDestination(String destination) {
            this.destination = destination;
            return this;
        }

        public FlightBuilder setPrice(int price) {
            this.price = price;
            return this;
        }

        public FlightBuilder setSeats(int seats) {
            this.seats = seats;
            return this;
        }

        public FlightBuilder setFlightNumber(String flightNumber) {
            this.flightNumber = flightNumber;
            return this;
        }

        public FlightBuilder setPassengerReservationItemMap(Map<Passenger, ReservationItem> passengerReservationItemMap) {
            this.passengerReservationItemMap = passengerReservationItemMap;
            return this;
        }

        public FlightBuilder setTicketpool(List<Integer> ticketpool) {
            this.ticketpool = ticketpool;
            return this;
        }

        public Flight createFlight() {
            return new Flight(source, destination, price, seats, flightNumber, passengerReservationItemMap, ticketpool);
        }
    }


}

package com.airline.entity;

public enum FlightCSVEnum {
    FLIGHT_NUMBER(0),
    NUMBER_OF_SEATS(1),
    PRICE_PER_SEAT(2),
    ORIGIN(3),
    DESTINATION(4);

    /**
     * Index in CSV column.
     */
    private final int index;

    /**
     * Constructor.
     * @param index Index in CSV column.
     */
    private FlightCSVEnum(final int index) {
        this.index = index;
    }

    /**
     * Get Index.
     * @return index.
     */
    public int getIndex() {
        return index;
    }
}
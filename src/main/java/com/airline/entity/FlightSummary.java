package com.airline.entity;

public class FlightSummary {
    private final String flightNumber;

    /**
     * Seats available.
     */
    private final int availableSeats;

    /**
     * Sold seats.
     */
    private final int soldSeats;

    /**
     * Total avenue.
     */
    private final long totalAvenue;

    /**
     * Flight summary.
     */
    private final String summary;

    public FlightSummary(String flightNumber, int availableSeats, int soldSeats, long totalAvenue, String summary) {
        this.flightNumber = flightNumber;
        this.availableSeats = availableSeats;
        this.soldSeats = soldSeats;
        this.totalAvenue = totalAvenue;
        this.summary = summary;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public int getSoldSeats() {
        return soldSeats;
    }

    public long getTotalAvenue() {
        return totalAvenue;
    }

    public String getSummary() {
        return summary;
    }
}

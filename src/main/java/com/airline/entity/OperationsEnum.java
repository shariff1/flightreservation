package com.airline.entity;

public enum OperationsEnum {
    BOOK_PASSENGER("bookPassenger"),
    CHNAGE_PRICE("changePrice"),
    CANCEL_PASSENGER("cancelPassenger");

    private String transactionType;

    OperationsEnum(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionType() {
        return transactionType;
    }
}

package com.airline.entity;

public class ReservationItem {
    private Passenger passenger;
    private int seatnumber;
    private int price;

    public ReservationItem(Passenger passenger, int seatnumber, int price) {
        this.passenger = passenger;
        this.seatnumber = seatnumber;
        this.price = price;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public int getSeatnumber() {
        return seatnumber;
    }

    public int getPrice() {
        return price;
    }
}

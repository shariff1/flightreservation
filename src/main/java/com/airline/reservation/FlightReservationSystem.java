package com.airline.reservation;

import com.airline.calculation.SourceDestinationCombination;
import com.airline.entity.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class FlightReservationSystem {
    Map<SourceDestinationCombination, TreeSet<Flight>> searchFlightwithSourceDestination;
    Map<String, Flight> searchFlightWithFlightNumber;

    public FlightReservationSystem(final String pathForFlightInfo) {
        if (pathForFlightInfo == null) {
            throw new IllegalArgumentException("path for flight information is not found");

        }
        createFlightInformation(pathForFlightInfo);

    }

    void createFlightInformation(String pathForFlightInfo) {
        searchFlightwithSourceDestination = new HashMap<SourceDestinationCombination, TreeSet<Flight>>();
        searchFlightWithFlightNumber = new HashMap<String, Flight>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(pathForFlightInfo));
            String detail = bufferedReader.readLine();
            while (detail != null) {
                extractFlightDetail(detail.replaceAll("\\s", "").split(","));
                detail = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void extractFlightDetail(String[] flightDetail) {
        String flightNUmber = flightDetail[FlightCSVEnum.FLIGHT_NUMBER.getIndex()];
        int numberofSeats = Integer.parseInt(flightDetail[FlightCSVEnum.NUMBER_OF_SEATS.getIndex()]);
        int costofSeat = Integer.parseInt(flightDetail[FlightCSVEnum.PRICE_PER_SEAT.getIndex()]);
        String source = flightDetail[FlightCSVEnum.ORIGIN.getIndex()];
        String destination = flightDetail[FlightCSVEnum.DESTINATION.getIndex()];
        SourceDestinationCombination sourceDestinationCombination =
                new SourceDestinationCombination(source, destination);
        TreeSet<Flight> flights = searchFlightwithSourceDestination.get(sourceDestinationCombination);
        if (flights == null) {
            flights = new TreeSet<Flight>();
        }
        Flight flight = new Flight.FlightBuilder()
                .setSource(source).setDestination(destination).
                        setPrice(costofSeat).setSeats(numberofSeats).
                        setFlightNumber(flightNUmber).createFlight();
        flights.add(flight);
        searchFlightwithSourceDestination.put(sourceDestinationCombination, flights);
        Flight temp = searchFlightWithFlightNumber.get(flightNUmber);
        if (temp == null) {
            searchFlightWithFlightNumber.put(flightNUmber, flight);
        }

    }

    public void handleTranscation(String pathForPersonDetail) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(pathForPersonDetail));
            String personDetail = bufferedReader.readLine();
            while (personDetail != null) {
                processTransaction(personDetail.replaceAll("\\s", "").split(","));
                personDetail = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processTransaction(String[] operationDetails) {
        String operation = operationDetails[0];
        if (OperationsEnum.BOOK_PASSENGER.getTransactionType().equals(operation)) {
            bookPassenger(operationDetails);
        } else if (OperationsEnum.CHNAGE_PRICE.getTransactionType().equals(operation)) {
            changePrice(operationDetails);
        } else if (OperationsEnum.CANCEL_PASSENGER.getTransactionType().equals(operation)) {
            cancelPassenger(operationDetails);
        }
    }

    private void cancelPassenger(String[] operationDetails) {
        Passenger passenger = new Passenger(operationDetails[1]);
        TreeSet<Flight> flights = getFlightByOriginandDestination(operationDetails);
        for (Flight flight : flights) {
            ReservationItem reservation = flight.getReservationByPassenger(passenger);
            if (reservation == null) {
                continue;
            } else {
                flights.add(flight);
            }

        }
        if (flights.size() != 0) {
            Flight expensiveflight = flights.last();
            ReservationItem reservationItem = expensiveflight.getReservationByPassenger(passenger);
            expensiveflight.cancelPassenger(reservationItem);
            expensiveflight.recoverSeat(reservationItem.getSeatnumber());
        }


    }

    private void changePrice(String[] operationDetails) {
        int newPrice = Integer.parseInt(operationDetails[2]);
        String flightNumber = operationDetails[1];
        Flight flight = searchFlightWithFlightNumber.get(flightNumber);
        if (flight == null) {
            return;
        }
        SourceDestinationCombination sourceDestinationCombination = new SourceDestinationCombination(
                flight.getSource(), flight.getDestination()
        );
        TreeSet<Flight> flights = searchFlightwithSourceDestination.get(sourceDestinationCombination);
        flights.remove(flight);
        flight.changePrice(newPrice);
        flights.add(flight);


    }

    private void bookPassenger(String[] operationDetails) {
        Passenger passenger = new Passenger(operationDetails[1]);
        TreeSet<Flight> flights = getFlightByOriginandDestination(operationDetails);
        if (flights == null) {
            throw new IllegalArgumentException("source or destination wrong");
        }
        for (Flight flight : flights) {
            if (flight.isFull()) {
                continue;

            } else {
                ReservationItem reservationItem = new ReservationItem(passenger, flight.getRandomSeats(), flight.getPriceforSeat());
                flight.bookPassenger(reservationItem);

            }
        }


    }

    private TreeSet<Flight> getFlightByOriginandDestination(String[] operationDetails) {
        String source = operationDetails[2];
        String destination = operationDetails[3];
        SourceDestinationCombination sourceDestinationCombination =
                new SourceDestinationCombination(source, destination);
        return searchFlightwithSourceDestination.get(sourceDestinationCombination);

    }

    public void createOutput(final String outputFilePath) throws IOException {
        StringBuffer sBuffer = new StringBuffer();
        int totalSeatsSold = 0;
        long totalRevenue = 0;
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFilePath));
        try {
            for (Map.Entry<String, Flight> entry : searchFlightWithFlightNumber.entrySet()) {
                Flight flight = entry.getValue();
                FlightSummary summary = flight.summaryFlight();
                sBuffer.append(summary.getSummary())
                        .append("\n");
                totalSeatsSold += summary.getSoldSeats();
                totalRevenue += summary.getTotalAvenue();
            }
            sBuffer.append("\n")
                    .append("System's summary")
                    .append("\n")
                    .append("Total seats sold: ")
                    .append(totalSeatsSold)
                    .append("\n")
                    .append("Total revenue: $")
                    .append(String.valueOf(totalRevenue));
            bw.write(sBuffer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
